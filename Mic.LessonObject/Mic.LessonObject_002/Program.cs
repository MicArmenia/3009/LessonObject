﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_002
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                age = 19
            };

            Student st1 = new Student();
            st1.name = "A1yan";
            st1.surname = "A1";
            st1.email = "a1@gmail.com";
            st1.age = 19;
        }
    }
}
