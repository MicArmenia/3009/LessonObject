﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_001
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st1 = new Student("A1", "A1yan", "a1@gmail.com", 10);
            Student st2 = new Student(
                name: "A1", 
                surname: "A1yan", 
                email: "a1@gmail.com", 
                age: 10);
        }
    }
}
