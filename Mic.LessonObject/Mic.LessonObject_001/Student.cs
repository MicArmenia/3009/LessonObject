﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_001
{
    class Student
    {
        public Student()
        {
            age = 10;
        }

        public Student(string name) : this()
        {
            if (!string.IsNullOrEmpty(name) && char.IsLetter(name[0]))
                this.name = name;
            else
                this.name = "";
        }

        public Student(string name, string surname) : this(name)
        {
            this.surname = surname;
        }
        public Student(string name, string surname, string email) : this(name, surname)
        {
            this.email = email;
        }
        public Student(string name, string surname, string email, int age) : this(name, surname, email)
        {
            this.age = age;
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}