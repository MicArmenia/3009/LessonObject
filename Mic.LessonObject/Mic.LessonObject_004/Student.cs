﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_004
{
    class Student
    {
        public Student(string email)
        {
            this.email = email;
        }

        public string name;
        public string surname;
        public readonly string email;
        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if (value < 15 || value > 75)
                    age = 0;
                else
                    age = value;
            }
        }

        //public int get_Age()
        //{
        //    return age;
        //}

        //public void set_Age(byte value)
        //{
        //    if (value < 15 || value > 75)
        //        age = 0;
        //    else
        //        age = value;
        //}
    }
}