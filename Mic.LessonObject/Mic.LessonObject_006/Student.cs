﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_006
{
    class Student
    {
        public Student(string email)
        {
            this.email = email;
        }

        //private string surname;

        //public string Surname
        //{
        //    get { return surname; }
        //    set { surname = value; }
        //}
        public string Surname { get; set; }

        public string FullName => $"{name} {Surname}";
        //public string FullName
        //{
        //    get { return $"{name} {Surname}"; }
        //}

        public readonly string email;

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (!string.IsNullOrEmpty(value) && char.IsLetter(value[0]))
                    name = value;
                else
                    name = "";
            }
        }

        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if (value < 15 || value > 75)
                    age = 0;
                else
                    age = value;
            }
        }

        //public int get_Age()
        //{
        //    return age;
        //}

        //public void set_Age(byte value)
        //{
        //    if (value < 15 || value > 75)
        //        age = 0;
        //    else
        //        age = value;
        //}
    }
}