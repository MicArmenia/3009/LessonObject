﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonObject_006
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student("a1@gmail.com")
            {
                Name = "Artyom",
                Surname = "Tonoyan",
                Age = 250
            };

            //Console.WriteLine($"{st.Name} {st.Surname}");
            Console.WriteLine(st.FullName);
            Console.ReadLine();
        }
    }
}
